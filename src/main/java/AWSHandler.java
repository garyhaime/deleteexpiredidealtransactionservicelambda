import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import service.IDealTransactionService;

import java.util.Map;


public class AWSHandler implements RequestHandler<Map<String, String>, String> {
        LambdaLogger logger;

        @Override
        public String handleRequest(Map<String, String> input, Context context){
            String response = null;
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
//          logger.log(System.getenv("DB_URL")); // deze is alleen aan te roepen als dit project als Lambda is geupload

            String operationType = input.get("operation");
            IDealTransactionService ds = new IDealTransactionService();
            switch(operationType) {
                case "get":
                    response = gson.toJson(ds.getExpiredIDealtransactionsService());
                    break;
                case "delete":
                    response = gson.toJson(ds.deleteExpiredIDealTransactionsService());
                    break;
                default:
                    response = "Invalid operation";
                    break;
            }
            return response + " rows affected";
        }
}