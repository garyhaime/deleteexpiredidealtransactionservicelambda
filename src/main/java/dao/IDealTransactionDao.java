package dao;

import model.IDealTransaction;
import service.IDealTransactionService;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IDealTransactionDao {
    public Connection conn;

    public IDealTransactionDao(){
        getConnection();
    }

    public Connection getConnection(){
        conn = null;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
/*op kantoor*/conn = DriverManager.getConnection("jdbc:mysql://cbomiscdev.cqhwymc6f0gv.us-east-1.rds.amazonaws.com:3306/cbomisc_dev",
                    "cbomisc_dev", "ahdoo3Sh");
/*thuis*/  // conn = DriverManager.getConnection("jdbc:mysql://85.158.166.196:3306/cbomisc_dev", "cbomisc_dev", "ahdoo3Sh");
/*werkend in Lambda?!*///conn = DriverManager.getConnection(System.getenv("DB_URL"), "DB_USER", "DB_PASSWORD");
            return conn;
        } catch (Exception e) {
            e.printStackTrace();
            return conn;
        }
    }

    public void closeConnection(){
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int deleteExpiredIDealTransactionsBefore(Date date){
        int rowsAffected = 0;
        try {
            String queryForDeletion = "DELETE FROM IDEAL_TRANSACTION_COPY WHERE ORDER_DATE < ?";
            PreparedStatement ps = conn.prepareStatement(queryForDeletion);
            ps.setDate(1, date);
            rowsAffected = ps.executeUpdate();
            conn.close();
            return rowsAffected;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public List<IDealTransaction> getExpiredIdealTransactionsBefore(Date date){
        String queryForDisplay = "SELECT * FROM IDEAL_TRANSACTION_COPY WHERE ORDER_DATE < ?";
        List<IDealTransaction> resultsTable = new ArrayList<>();
        //
        int testCount = 0; //alleen voor testen!
        try {
            PreparedStatement ps = conn.prepareStatement(queryForDisplay); // <-- (misschien) aanpassen naar specifieke kolommen voor snellere query
            ps.setDate(1, date);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int ID = rs.getInt(1);
                int orderID = rs.getInt(4);
                Date orderDate = rs.getDate(5);
                String emailAddress = rs.getString(7);
                IDealTransaction iDealTransaction = new IDealTransaction(ID, orderID, orderDate, emailAddress);
                resultsTable.add(iDealTransaction);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultsTable;
    }
}