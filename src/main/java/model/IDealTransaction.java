package model;

import java.sql.Date;

public class IDealTransaction {
    int ID;
    int orderID;
    Date orderDate;
    String emailAddress;

    public IDealTransaction(int ID, int orderID, Date orderDate, String emailAddress) {
        this.ID = ID;
        this.orderID = orderID;
        this.orderDate = orderDate;
        this.emailAddress = emailAddress;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
}
