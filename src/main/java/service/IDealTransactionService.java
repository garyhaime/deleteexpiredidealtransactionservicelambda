package service;
import dao.IDealTransactionDao;
import model.IDealTransaction;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class IDealTransactionService {

    public List<IDealTransaction> getExpiredIDealtransactionsService(){
        IDealTransactionDao ds = new IDealTransactionDao();
        int olderThanMonths = 36;
        Date expiredDate = java.sql.Date.valueOf(LocalDate.now().minusMonths(olderThanMonths));
        List<IDealTransaction> list = ds.getExpiredIdealTransactionsBefore(expiredDate);
        return list;
    }

    //MAAK HIER GETSELECTEXPIREDIDEALTREANSACITONSERVICE AAN

    public int deleteExpiredIDealTransactionsService(){
        IDealTransactionDao ds = new IDealTransactionDao();
        int olderThanMonths = 36;
        Date expiredDate = java.sql.Date.valueOf(LocalDate.now().minusMonths(olderThanMonths));
        int rowsAffected = ds.deleteExpiredIDealTransactionsBefore(expiredDate);
        return rowsAffected;
    }
}
